# Link obfuscation

The Link Obfuscation module enhances privacy by obfuscating facets on search pages, converting `<a>` tags into `<span>` elements.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Edit the facet you want to obfuscate and check the 'Obfuscate facet link' checkbox (/admin/config/search/facets/{facet_name}/edit).

You should update the CSS to target the new `span.drupal-masked-element` facets, as the module replaces the original `<a>` elements with these spans.
