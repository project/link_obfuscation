(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.link_obfuscation = {
    attach: function attach(context) {
      $(once('initConcealedLInk', 'span.drupal-masked-element')).on('click', function () {
        let span = $(this);
        let url = null;

        if (span.data('internal')) {
          let routeName = span.data('routeName');
          let routeParameters = span.data('routeParameters');

          let options = span.data('options');

          $.ajax({
            url: '/link-obfuscation/url',
            type: 'POST',
            data: {
              'route_name': routeName,
              'route_parameters': routeParameters,
              'options': options,
            },
            cache: false,
            success: function (data) {
              let response = JSON.parse(data);
              if (response.code != 200) {
                return;
              }

              url = response.url;
              clickUrl(span, url);
            },
          });
        } else {
          let encodedUrl = $(this).data('encodedName');

          if (typeof encodedUrl == "undefined") {
            return;
          }

          url = atob(encodedUrl);
          clickUrl(span, url);
        }
      });

      function clickUrl(span, url) {
        if (url == null) {
          return;
        }

        let link = document.createElement('a');
        link.href = url;

        if (typeof span.data('target') != "undefined") {
          link.target = span.data('target');
        }

        link.click();
      }

    }
  };
})(jQuery, Drupal, drupalSettings);
