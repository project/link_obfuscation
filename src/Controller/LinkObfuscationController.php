<?php

namespace Drupal\link_obfuscation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Obfuscation Link Controller.
 */
class LinkObfuscationController extends ControllerBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManager $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * Get clean uri from post request.
   */
  public function getCleanUri(Request $request) {
    $routeName = $request->get('route_name');
    $routeParameters = $request->get('route_parameters', []);
    $options = $request->get('options', []);

    try {
      if (!empty($options['language'])) {
        $options['language'] = $this->languageManager
          ->getLanguage($options['language']);
      }

      $url = Url::fromRoute($routeName, $routeParameters, $options)->setAbsolute(TRUE);

      $response = [
        'code' => 200,
        'url' => $url->toString(),
      ];
    }
    catch (\Exception $e) {
      $this->getLogger('link_obfuscation')->error($e);
      $response = [
        'code' => 500,
        'url' => NULL,
        'message' => 'There has been an error.',
      ];
    }

    return new Response(json_encode($response));
  }

}
