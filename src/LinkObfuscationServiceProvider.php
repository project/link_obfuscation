<?php

namespace Drupal\link_obfuscation;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\link_obfuscation\Service\ObfuscateLinkGenerator;

/**
 * Alters container services.
 */
class LinkObfuscationServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    parent::alter($container);

    $container->getDefinition('link_generator')
      ->setClass(ObfuscateLinkGenerator::class);
  }

}
